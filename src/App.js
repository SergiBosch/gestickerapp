import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import { Navbar, Nav, Button } from "react-bootstrap";
import Routes from "./Routes";
import "./App.css";

function App(props) {

  const [isAuthenticated, userHasAuthenticated] = useState("");
  const [tipo_usuario, setTipoUsuario] = useState("");

  useEffect(() => {
    async function onLoad() {
      fetch(`http://localhost:8082/session/`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(login => {
        userHasAuthenticated(login);
        setTipoUsuario(login.tipo_usuario.id)
      }).catch(err => {
        props.history.push('/login')
      });
    }

    onLoad();
  }, []);


  function handleLogout() {
    fetch(`http://localhost:8082/session/`, {
      method: 'DELETE',
      credentials: "include",
    }).then(response => {
      if (response.status === 200) {
        userHasAuthenticated("");
        setTipoUsuario(null)
        props.history.push("/login");
      }
    })
  }

  function menu() {
    switch (tipo_usuario) {
      case 1:
        return <>
          <Link to="/tipousuario/1/10">Tipo Usuario</Link>
          <Link to="/usuario/1/10">Usuarios</Link>
          <Link to="/facturaticket/1/10">Ticket</Link>
          <Link to="/lineafactura/1/10">LineaFactura</Link>
          <Link to="/producto/1/10">Producto</Link>
          <Link to="/tipoproducto/1/10">Tipo Producto</Link>
        </>;
      case 2:
        return <ul className="navbar-nav">
          <li class="nav-item">
            <Link className="nav-link" to="/usuario/1/10">Usuarios</Link>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/insert/usuario/">Nuevo Usuario</Link>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/mistickets">Mis Tickets</Link>
          </li>
          <li class="nav-item ">
            <Link className="nav-link" to="/facturaticket/1/10">Todos Los Tickets</Link>
          </li>
          <li class="nav-item ">
            <Link className="nav-link" to="/insert/facturaticket/">Nuevo Ticket</Link>
          </li>
          <li class="nav-item ">
            <Link className="nav-link" to="/producto/1/10">Producto</Link>
          </li>
          <li class="nav-item ">
            <Link className="nav-link" to="/insert/producto/">Nuevo Producto</Link>
          </li>
        </ul>;
      case 3:
        return <ul className="navbar-nav">
        <li class="nav-item">
          <Link className="nav-link" to="/mistickets/">Mis Tickets</Link>
          </li>
          <li class="nav-item">
          <Link className="nav-link" to="/insert/facturaticket/">Nuevo Ticket</Link>
          </li>
          <li class="nav-item">
          <Link className="nav-link" to="/insert/usuario/">Nuevo Cliente</Link>
          </li>
        </ul>;
      default:
        return;
    }
  }

  function userInfo() {
    return isAuthenticated.nombre + " " + isAuthenticated.apellido1 + " " + isAuthenticated.apellido2
  }

  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand><Link style={{ textDecoration: 'none', color: "white" }}  to="/">GesTicker App</Link></Navbar.Brand>
        {isAuthenticated ? (
          <Navbar.Collapse>
            {menu()}
          </Navbar.Collapse>) : (<> </>)}
        <Navbar.Collapse className="justify-content-end">
          {isAuthenticated ? (<>
            <span class="navbar-text">{userInfo()}</span>
                <Button className="btn btn-secondary" onClick={handleLogout} >Logout</Button>
          </>) : (<>
            <Link style={{ textDecoration: 'none', color: "white" }} to="/login">Login</Link>
          </>)
          }
        </Navbar.Collapse>
      </Navbar>
      <br></br>
      <div className="container">
        <Routes appProps={{ isAuthenticated, userHasAuthenticated, tipo_usuario, setTipoUsuario }} />
      </div>
    </div>
  );
}

export default withRouter(App);