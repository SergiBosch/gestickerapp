
import React from 'react';
import { Document, Page, Text, StyleSheet, View } from '@react-pdf/renderer';
import { PDFDownloadLink } from '@react-pdf/renderer';
import Button from 'react-bootstrap/Button'


class DownloadButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ticket: [],
            lineasfacturas: [],
            total: 0
        }
    }

    UNSAFE_componentWillMount() {
        console.log(this.props.ticket)
        this.setState({ ticket: this.props.ticket })
        this.setState({ lineasfacturas: this.props.ticket.listarLineas_factura })
    }

   /*UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({ ticket: nextProps.ticket })
        this.setState({ lineasfacturas: nextProps.ticket.listarLineas_factura })
    }*/

    render() {
        const styles = StyleSheet.create({
            body: {
                padding: 10,
                fontSize: 10
            },
            table: {
                padding: 5,
                display: "table",
                width: "auto",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 1,
                borderRightWidth: 1,
                borderBottomWidth: 1
            },
            tableRow: {
                margin: "auto",
                height: "25px",
                flexDirection: "row"
            },
            tableColHeader: {
                width: "50%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderBottomColor: '#000',
                borderWidth: 1,
                borderLeftWidth: 0,
                borderTopWidth: 0,
                borderRightWidth: 0
            },
            tableColHeader2: {
                width: "16.7%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderBottomColor: '#000',
                borderWidth: 1,
                borderLeftWidth: 0,
                borderTopWidth: 0,
                borderRightWidth: 0
            },
            tableCol: {
                width: "50%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 0,
                borderLeftWidth: 0,
                borderTopWidth: 0
            },
            tableCol2: {
                width: "16.6%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 0,
                borderLeftWidth: 0,
                borderTopWidth: 0
            },
            tableCellHeader: {
                margin: "auto",
                margin: 5,
                fontSize: 12,
                fontWeight: 500
            },
            tableCell: {
                margin: "auto",
                margin: 5,
                fontSize: 10
            },
            tableDatos: {
                padding: 5,
                display: "table",
                width: "auto",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 0
            },
            tableColDatos: {
                width: "33.3%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 0,
                borderLeftWidth: 0,
                borderTopWidth: 0
            },
            tableCellDatos: {
                margin: "auto",
                margin: 5,
                fontSize: 10,
                textAlign: "center"
            },
            tableRowDatos: {
                margin: "auto",
                height: "22px",
                flexDirection: "row"
            },
            separedor: {
                height: "50px",
            },
            separedor2: {
                height: "25px",
            },
            tableColFecha: {
                width: "100%",
                borderStyle: "solid",
                borderColor: '#bfbfbf',
                borderWidth: 0,
                borderLeftWidth: 0,
                borderTopWidth: 0
            }

        });

        const Quixote = () => (
            <Document>
                <Page style={styles.body}>
                    <View style={styles.separedor2}></View>
                    <View style={styles.tableDatos}>
                        <View style={styles.tableRowDatos}>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>Empresa S.A.</Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellHeader}></Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>{this.state.ticket.cliente.nombre + "" + this.state.ticket.cliente.apellido1 + "" + this.state.ticket.cliente.apellido2}</Text>
                            </View>
                        </View>
                        <View style={styles.tableRowDatos}>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>B-896341235-F</Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellHeader}></Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>Valencia, Valencia</Text>
                            </View>
                        </View>
                        <View style={styles.tableRowDatos}>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>Tel. 9610568412</Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellHeader}></Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>{this.state.ticket.cliente.dni}</Text>
                            </View>
                        </View>
                        <View style={styles.tableRowDatos}>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>empresa@gesticker.com</Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellHeader}></Text>
                            </View>
                            <View style={styles.tableColDatos}>
                                <Text style={styles.tableCellDatos}>{this.state.ticket.cliente.email}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.separedor}></View>
                    <View style={styles.separedor2}></View>
                    <View style={styles.tableDatos}>
                        <View style={styles.tableRowDatos}>
                            <View style={styles.tableColFecha}>
                                <Text style={styles.tableCell}>Fecha emisón: {new Date().toLocaleDateString()}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.table}>
                        <View style={styles.tableRow}>
                            <View style={styles.tableColHeader2}>
                                <Text style={styles.tableCellHeader}>Tipo</Text>
                            </View>
                            <View style={styles.tableColHeader}>
                                <Text style={styles.tableCellHeader}>Producto</Text>
                            </View>
                            <View style={styles.tableColHeader2}>
                                <Text style={styles.tableCellHeader}>Cantidad</Text>
                            </View>
                            <View style={styles.tableColHeader2}>
                                <Text style={styles.tableCellHeader}>Precio</Text>
                            </View>
                        </View>
                        {this.state.lineasfacturas.map(ticket => {
                            let nuevoTotal = ticket.precio*ticket.cantidad
                            this.state.total += nuevoTotal
                            return (
                                
                                <View style={styles.tableRow}>
                                    <View style={styles.tableCol2}>
                                        <Text style={styles.tableCell}>{ticket.producto.tipo_producto.descripcion}</Text>
                                    </View>
                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}>{ticket.producto.descripcion}</Text>
                                    </View>
                                    <View style={styles.tableCol2}>
                                        <Text style={styles.tableCell}>{ticket.cantidad}</Text>
                                    </View>
                                    <View style={styles.tableCol2}>
                                        <Text style={styles.tableCell}>{ticket.precio}€</Text>
                                    </View>
                                </View>
                            );
                        })}
                    </View>
                    <View style={styles.tableRow}>
                        <View style={styles.tableCol2}>
                            <Text style={styles.tableCell}></Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}></Text>
                        </View>
                        <View style={styles.tableCol2}>
                            <Text style={styles.tableCell}>Total:</Text>
                        </View>
                        <View style={styles.tableCol2}>
                    <Text style={styles.tableCell}>{this.state.total}€</Text>
                        </View>
                    </View>
                </Page>
            </Document>
        );

        const GeneratePdf = () => (
            <div>
                <PDFDownloadLink document={<Quixote />} fileName={'Factura' + this.state.ticket.cliente.nombre + '-' + new Date().toLocaleDateString() + '.pdf'}>
                    {({ blob, url, loading, error }) => (loading ? (
                        <Button className="btn btn-secondary disabled"><i class="fa fa-download" aria-hidden="true"></i></Button>
                    ) : (
                            <Button className="btn btn-secondary"><i class="fa fa-download" aria-hidden="true"></i></Button>
                        ))}
                </PDFDownloadLink>
            </div>
        )
        return (
            <div>
                <h4>
                    {<GeneratePdf />}
                </h4>
            </div>
        );
    }
}

export default DownloadButton;