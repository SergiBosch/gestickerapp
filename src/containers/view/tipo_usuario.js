import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Login(props) {
  const [descripcion, setDescripcion] = useState("");

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/tipousuario/get/${params.id}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(login => {
        setDescripcion(login.descripcion)
      }).catch(err => err);
    }

    onLoad();
  }, [props]);

  return (
    <div className="Login">
        <Form>
          <Form.Group controlId="descripcion" bsSize="large">
            <Form.Label>Descripcion</Form.Label>
            <p>{descripcion}</p>
          </Form.Group>
          <Link block className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </Form>
    </div>
  );
}