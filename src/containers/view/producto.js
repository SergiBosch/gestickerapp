import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Login(props) {
  const [descripcion, setDescripcion] = useState("");
  const [precio, setPrecio] = useState("");

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/producto/get/${params.id}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(datos => {
        setDescripcion(datos.descripcion)
        setPrecio(datos.precio)
      }).catch(err => err);
    }

    onLoad();
  }, [props]);

  return (
    <div className="Login">
      <Form>
        <Form.Label>Descripcion</Form.Label>
        <p>{descripcion}</p>
        <Form.Label>Precio</Form.Label>
        <p>{precio}€</p>
        <br></br>
      <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
      </Form>
    </div>
  );
}