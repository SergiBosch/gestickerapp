import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Login(props) {
  const [dni, setDni] = useState("");
  const [nombre, setNombre] = useState("");
  const [apellido1, setApellido1] = useState("");
  const [apellido2, setApellido2] = useState("");
  const [email, setEmail] = useState("");
  const [login, setLogin] = useState("");
  const [tipousuario, setTipousuario] = useState(""); 

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/usuario/get/${params.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        setDni(login.dni)
        setNombre(login.nombre)  
        setApellido1(login.apellido1)   
        setApellido2(login.apellido2)   
        setEmail(login.email)   
        setLogin(login.login)
        setTipousuario(login.tipo_usuario)   
      }).catch(err => err);
    }

    onLoad();
  }, [props]);

  return (
    <div>
      <Form>
        <div className="row">
        <Form.Group controlId="dni" bsSize="large" className="col-md-6">
          <Form.Label>DNI</Form.Label>
          <p>{dni}</p>
        </Form.Group>
        </div>
        <div className="row">
        <Form.Group controlId="nombre" bsSize="large" className="col-md-4">
          <Form.Label>Nombre</Form.Label>
          <p>{nombre}</p>
        </Form.Group>
        <Form.Group controlId="apellido1" bsSize="large" className="col-md-4">
          <Form.Label>Primer Apellido</Form.Label>
          <p>{apellido1}</p>
        </Form.Group>
        <Form.Group controlId="apellido2" bsSize="large" className="col-md-4">
          <Form.Label>Segundo Apellido</Form.Label>
          <p>{apellido2}</p>
        </Form.Group>
        </div>
        <div className="row">
        <Form.Group controlId="email" bsSize="large" className="col-md-6">
          <Form.Label>Email</Form.Label>
          <p>{email}</p>
        </Form.Group>
        <Form.Group controlId="login" bsSize="large" className="col-md-6">
          <Form.Label>Username</Form.Label>
          <p>{login}</p>
        </Form.Group>
        </div>
        <Form.Group controlId="tipousuario" bsSize="large">
          <Form.Label>Tipo Usuario</Form.Label>
          <p>{tipousuario.descripcion}</p>
        </Form.Group>
        <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
      </Form>
    </div>
  );
}