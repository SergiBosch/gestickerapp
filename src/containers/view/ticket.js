import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table'


export default function Login(props) {
  const [id, setId] = useState(""); 
  const [titulo, setTitulo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [fechainicio, setFechainicio] = useState("");
  const [fechafin, setFechafin] = useState("");
  const [cliente, setCliente] = useState("");
  const [trabajador, setTrabajador] = useState("");
  const [lineafactura, setLineafactura] = useState([]);
  const { match: { params } } = props;
  

  useEffect(() => {
    async function onLoad() {
      fetch(`http://localhost:8082/facturaticket/get/${params.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        setId(login.id)
        setTitulo(login.titulo)
        setDescripcion(login.descripcion)
        setFechainicio(login.fecha_alta)
        setFechafin(login.fecha_fin)  
        setCliente(login.cliente)   
        setTrabajador(login.trabajador)   
      }).catch(err => err);


      fetch(`http://localhost:8082/lineafactura/factura/${params.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        setLineafactura(login) 
      }).catch(err => err);
      
    }

    onLoad();
  }, [params]);

  function handleSubmit(event) {
    event.preventDefault();
    fetch(`http://localhost:8082/facturaticket/cerrar/${params.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        props.history.push("/");
      }).catch(err => err);
  }

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <div className="row">
          <h2>{titulo}</h2>
          </div>
          <br/>
          <div className="row">
        <Form.Group controlId="fechainicio" bsSize="large" className="col-md-3 col-xs-6">
          <Form.Label>Fecha Alta:</Form.Label>
          <p>{new Date(fechainicio).toLocaleDateString()}</p>
        </Form.Group>
        <Form.Group controlId="fechafin" bsSize="large" className="col-md-3 col-xs-6">
          <Form.Label>Fecha Cierre:</Form.Label>
          <p>{fechafin ? (new Date(fechafin).toLocaleDateString()):("Sin cerrar")}</p>
        </Form.Group>
        <Form.Group controlId="cliente" bsSize="large" className="col-md-3">
          <Form.Label>Cliente:</Form.Label>
          <p>{cliente.nombre} {cliente.apellido1} {cliente.apellido2}</p>
        </Form.Group>
        <Form.Group controlId="trabajador" bsSize="large" className="col-md-3">
          <Form.Label>Asignado a:</Form.Label>
          <p>{trabajador.nombre} {trabajador.apellido1} {trabajador.apellido2}</p>
        </Form.Group>
        </div>
        <div className="row">
        <h4>Descripcion del trabajo:</h4>
        </div>
        <div className="row">
        <p>{descripcion}</p>
        </div>
        <h4>Lista de materiales: <Link className="btn btn-secondary" to={{ pathname: `/insert/lineafactura/${id}` }}>Añadir Linea Factura</Link></h4>
        <Table striped bordered hover>
          <thead>
          <tr>
            <th>Tipo Producto</th>
            <th>Producto</th>  
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
          {lineafactura.map(lineasfactura => {
              return (
                <tr key={lineasfactura.id}>
                  <td>{lineasfactura.producto.tipo_producto.descripcion}</td>
                  <td>{lineasfactura.producto.descripcion}</td>
                  <td>{lineasfactura.cantidad}</td>
                  <td>{lineasfactura.precio}</td>                  
                  <td>
                    <div className="btn-group">
                      <Link className = "btn btn-secondary" to={{ pathname: `/delete/lineafactura/${lineasfactura.id}` }}><i className="fas fa-trash"></i></Link>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <div className="row text-center">
        
        <Button class="list-group-item col" type="submit">
            Cerrar Ticket
        </Button>
        <div class="col"></div>
        <Link className="btn btn-secondary" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </Form>
    </div>
  );
}