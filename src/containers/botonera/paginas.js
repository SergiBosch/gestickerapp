import React, { Component } from 'react';
import { Link } from "react-router-dom";

class paginas extends Component {
    constructor(props) {
        super(props);  
        this.state = {
            paginacion: []
          }     
    }

    UNSAFE_componentWillReceiveProps() {
        fetch(`http://localhost:8082/${this.props.tabla}/count`)
        .then((response) => {
          return response.json()
        })
        .then((registros) => {
            var paginas = []
            var paginaActual = parseInt(this.props.paginaActual)
            var vecindad = 3
            var numPaginas = Math.ceil(registros/this.props.rpp)
            for(var i=0; i < numPaginas; i++){
                var pagina = i+1
                if(pagina === 1){
                    paginas.push(pagina) 
                } else if (pagina > (paginaActual - vecindad) && pagina < (paginaActual + vecindad)){
                    paginas.push(pagina)
                } else if (pagina === numPaginas){
                    paginas.push(pagina)
                } else if (pagina === (paginaActual - vecindad) || pagina === (paginaActual + vecindad) ){
                    paginas.push("...")
                }
            }
            this.setState({paginacion: paginas})
        }).catch(err => {
            if (err.name === "AbortError") return
            throw err
          })
    }

    render(){
        var i = 0;
        return (
            <ul className="pagination">
                {this.state.paginacion.map(pagina => {
                    i++
                    return (
                        <li key={i}  className= {`page-item ${parseInt(this.props.paginaActual) === pagina ? "active" : ""} ${pagina === "..." ? "disabled" : ""}`}>
                            <Link className = {`page-link  `} to={{ pathname: `/${this.props.tabla}${this.props.paciente ? ("/" + this.props.paciente) : ("")}/${pagina}/${this.props.rpp}${this.props.filter ? ("/" + this.props.filter) : ("")}${this.props.col ? ("/" + this.props.col) : ("")}${this.props.order ? ("/" + this.props.order) : ("")}` }}>
                                {pagina} 
                            </Link>
                        </li>
                    );
                })}
            </ul>
          );
    }
}

export default paginas;