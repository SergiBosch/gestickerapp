import React, { Component } from 'react';
import { Link } from "react-router-dom";

class remove extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ tabla: params.tabla })
    fetch(`http://localhost:8082/${params.tabla}/${params.id}`,
      { method: 'delete' })
      .then((response) => {
        return response.json()
      })
      .then((resultado) => {
        if (resultado) {
          this.setState({ filas: "Borrado con exito" })
        }
      })
  }

  render() {
    return (
      <div>
        <div className="alert alert-success text-center">
          <h2>{this.state.filas}</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/${this.state.tabla}/1/10` }}>Volver</Link>
        </div>
      </div>
    );
  }
}

export default remove;