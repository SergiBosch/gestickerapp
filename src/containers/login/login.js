import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

import "./login.css";

export default function Login(props) {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return login.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = new FormData(event.target)
    fetch(`http://localhost:8082/session/`,{
      method: 'POST',
      body: data,
      credentials: "include",
    }).then( response => {
      return response.json();   
    }).then( login => {
      props.userHasAuthenticated(login);   
      props.setTipoUsuario(login.tipo_usuario.id)
      props.history.push("/");  
    }).catch(err => err);
  }

  function admin(){
    setLogin("admin")
    setPassword("admin")
  }
  function gerente(){
    setLogin("gerente")
    setPassword("gerente")
  }
  function tecnico(){
    setLogin("tecnico")
    setPassword("tecnico")
  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="login" bsSize="large">
          <Form.Label>Username</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            size="lg"
            value={login}
            name="login"
            onChange={e => setLogin(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password" bsSize="large">
          <Form.Label>Password</Form.Label>
          <Form.Control
            value={password}
            size="lg"
            onChange={e => setPassword(e.target.value)}
            type="password"
            name="password"
          />
        </Form.Group>
        <Button variant="outline-primary" size="lg" block disabled={!validateForm()} type="submit">
          Login
        </Button>
{/*}        <Button variant="outline-primary" size="lg" block onClick={admin} type="submit">
          Admin
        </Button>
        <Button variant="outline-primary" size="lg" block onClick={gerente} type="submit">
          Gerente
        </Button>
        <Button variant="outline-primary" size="lg" block onClick={tecnico} type="submit">
          Tecnico
  </Button>{*/}
      </Form>
    </div>
  );
}