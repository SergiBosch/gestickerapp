import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from 'react-select';



export default function Login(props) {
  const [titulo, setTitulo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [cliente, setCliente] = useState("");
  var [trabajador2, setTrabajador] = useState("");
  const [selectClientes, setSelectClientes] = useState("");
  const [selectTrabajadores, setSelectTrabajadores] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const validString = RegExp(/^[a-zA-Z ]{2,20}$/i);
  

  useEffect(() => {
    async function valorSelect() {
      fetch(`http://localhost:8082/usuario/getclientes`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( lista => {
        var itemsSelect = []
        lista.map( elemento =>
          itemsSelect.push({label: elemento.nombre+" "+elemento.apellido1+" "+elemento.apellido2, value: elemento.id})
        )
        setSelectClientes(itemsSelect)  
      }).catch(err => err);

      fetch(`http://localhost:8082/usuario/gettrabajadores`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( lista => {
        var itemsSelect = []
        lista.map( elemento =>
          itemsSelect.push({label: elemento.nombre+" "+elemento.apellido1+" "+elemento.apellido2, value: elemento.id})
        )
        setSelectTrabajadores(itemsSelect)  
      }).catch(err => err);
    }
    setTrabajador(props.isAuthenticated.id)
    valorSelect();
  }, [props]);


  function validateForm() {
    return validString.test(titulo) && validString.test(descripcion) && cliente;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      titulo: titulo,
      descripcion: descripcion,
      cliente: cliente,
      trabajador: trabajador2,
      fechainicio: new Date().toString(),
      fechafin: ''
    }
    console.log(data)
    fetch(`http://localhost:8082/facturaticket/`,{
      method: 'POST',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
    }).then( response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then( login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      {operacion ? (<>
      <Form onSubmit={handleSubmit}>
        <div className="row">
        <Form.Group controlId="fechainicio" bsSize="large" className="col-md-12" validationState={!validString.test(titulo) && titulo.length > 0 ? ("error"):("")}>
          <Form.Label>Titulo</Form.Label>
          <Form.Control
            value={titulo}
            onChange={e => setTitulo(e.target.value)}
            type="text"
            name="titulo"
          />
        </Form.Group>
        </div>
        <div className="row">
        <Form.Group controlId="cliente" bsSize="large" className="col-md-6">
          <Form.Label>Cliente</Form.Label>
          <Select options={selectClientes} onChange={e => setCliente(e.value)}/>
        </Form.Group>
        {props.tipo_usuario === 3 ? (<></>) : (<>
        <Form.Group controlId="trabajador" bsSize="large" className="col-md-6">
          <Form.Label>Trabajador Asignado</Form.Label>
          <Select options={selectTrabajadores} onChange={e => setTrabajador(e.value)}/>
        </Form.Group>
        </>)}
        </div>
        <div className="row">
        <Form.Group controlId="total" bsSize="large" className="col-md-12" validationState={!validString.test(descripcion) && descripcion.length > 0 ? ("error"):("")}>
          <Form.Label>Descripcion</Form.Label>
          <Form.Control value={descripcion} componentClass="textarea" onChange={e => setDescripcion(e.target.value)}/>
        </Form.Group>
        </div>
        <Button block bsSize="large" disabled={!validateForm()} type="submit">
          Crear
        </Button>
      </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos insertados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}