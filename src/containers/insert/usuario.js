import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import Select from 'react-select';
import { Link } from "react-router-dom";


export default function Login(props) {
  const [dni, setDni] = useState("");
  const [telefono, setTelefono] = useState("");
  const [nombre, setNombre] = useState("");
  const [apellido1, setApellido1] = useState("");
  const [apellido2, setApellido2] = useState("");
  const [email, setEmail] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [tipousuario, setTipousuario] = useState("");
  const [valoresSelect, setValoresselect] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const validDNI = RegExp(/^\d{8}[a-zA-Z]{1}$/i);
  const validTel = RegExp(/^\d{9}$/i);
  const validString = RegExp(/^[a-zA-Z ]{2,20}$/i);
  const validEmail = RegExp(/^\w+@\w+\.[a-zA-Z]{2,4}$/i);
  const validUsername = RegExp(/^\w{2,20}$/i);

  useEffect(() => {

    async function onLoad() {
      fetch(`http://localhost:8082/tipousuario/getall/`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(lista => {
        var itemsSelect = []
        lista.map(elemento => {
          if(props.tipo_usuario !== 1 && elemento.id !== 1){
            itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
          }
        })
        setValoresselect(itemsSelect)
      }).catch(err => err);
    }

    if(props.tipo_usuario === 3){
      setTipousuario(4)
    }

    onLoad();
  },[props]);


  function validateForm() {
    return (validDNI.test(dni) && validTel.test(telefono) && validString.test(nombre) && validString.test(apellido1) && validString.test(apellido2) && validEmail.test(email)) || (validUsername.test(login) && password.length > 6);
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      dni: dni,
      nombre: nombre,
      apellido1: apellido1,
      apellido2: apellido2,
      email: email,
      telefono: telefono,
      login: login,
      password: password,
      tipousuario: tipousuario
    }
    console.log(data)
    fetch(`http://localhost:8082/usuario/`, {
      method: 'POST',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
          {props.tipo_usuario === 3 ? (<></>) : (<>
            <Form.Group controlId="tipousuario" bsSize="large">
            <Form.Label>Tipo Usuario</Form.Label>
            <Select options={valoresSelect} onChange={e => setTipousuario(e.value)} />
          </Form.Group>
          </>)}
          
          <div className="row">
            <Form.Group controlId="dni" bsSize="large" className="col-md-6" validationState={!validDNI.test(dni) && dni.length > 0 ? ("error"):("")}>
              <Form.Label>DNI</Form.Label>
              <Form.Control
                value={dni}
                onChange={e => setDni(e.target.value)}
                type="text"
                name="dni"
              />
            </Form.Group>
            <Form.Group controlId="dni" bsSize="large" className="col-md-6" validationState={!validTel.test(telefono) && telefono.length > 0 ? ("error"):("")}>
              <Form.Label>Tel. Contacto</Form.Label>
              <Form.Control
                value={telefono}
                onChange={e => setTelefono(e.target.value)}
                type="text"
                name="telefono"
              />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="nombre" bsSize="large" className="col-md-4" validationState={!validString.test(nombre) && nombre.length > 0 ? ("error"):("")}>
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                value={nombre}
                onChange={e => setNombre(e.target.value)}
                type="text"
                name="nombre"
              />
            </Form.Group>
            <Form.Group controlId="apellido1" bsSize="large" className="col-md-4" validationState={!validString.test(apellido1) && apellido1.length > 0 ? ("error"):("")}>
              <Form.Label>Primer Apellido</Form.Label>
              <Form.Control
                value={apellido1}
                onChange={e => setApellido1(e.target.value)}
                type="text"
                name="apellido1"
              />
            </Form.Group>
            <Form.Group controlId="apellido2" bsSize="large" className="col-md-4" validationState={!validString.test(apellido2) && apellido2.length > 0 ? ("error"):("")}>
              <Form.Label>Segundo Apellido</Form.Label>
              <Form.Control
                value={apellido2}
                onChange={e => setApellido2(e.target.value)}
                type="text"
                name="apellido2"
              />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="email" bsSize="large" className="col-md-4" validationState={!validEmail.test(email) && email.length > 0 ? ("error"):("")}>
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="text"
                name="email"
              />
            </Form.Group>
            <Form.Group controlId="login" bsSize="large" className="col-md-4" validationState={!validUsername.test(login) && login.length > 0 ? ("error"):("")}>
              <Form.Label>Username</Form.Label>
              <Form.Control
                value={login}
                onChange={e => setLogin(e.target.value)}
                disabled={tipousuario === 4}
                type="text"
                name="login"
              />
            </Form.Group>
            <Form.Group controlId="password" bsSize="large" className="col-md-4" validationState={!password.length > 6 ? ("error"):("")}>
              <Form.Label>Contraseña</Form.Label>
              <Form.Control
                value={password}
                onChange={e => setPassword(e.target.value)}
                disabled={tipousuario === 4}
                type="password"
                name="password"
              />
            </Form.Group>
          </div>

          <Button block bsSize="large" disabled={!validateForm()} type="submit">
            Crear
        </Button>
        </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos insertados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}