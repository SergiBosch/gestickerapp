import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from 'react-select';


export default function Login(props) {
  const [facturaId, setFacturaId] = useState("");
  const [cantidad, setCantidad] = useState("");
  const [producto, setProducto] = useState("");
  const [valoresSelect, setValoresselect] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const validCantidad = RegExp(/^\d+$/i);

  useEffect(() => {
    setFacturaId(props.match.params.idfactura)
    async function onLoad() {
      fetch(`http://localhost:8082/producto/getall`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(lista => {
        var itemsSelect = []
        lista.map(elemento => 
            itemsSelect.push({ label: elemento.descripcion, value: elemento.id })        
        )
        setValoresselect(itemsSelect) 
      }).catch(err => err);
    }

    onLoad();
  }, [props]);

  
  function validateForm() {
    return validCantidad.test(cantidad) && producto;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      factura: facturaId,
      cantidad: cantidad,
      producto: producto
    }
    console.log(data)
    fetch(`http://localhost:8082/lineafactura/`, {
      method: 'POST',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      setResultado(login)
      setOperacion(false);
    }).catch(err => err);
  }

  return (
    <div className="Login">
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
        <Form.Group controlId="tipousuario" bsSize="large">
            <Form.Label>Producto</Form.Label>
            <Select options={valoresSelect} onChange={e => setProducto(e.value)} />
          </Form.Group>
          <Form.Group controlId="cantidad" bsSize="large"  validationState={!validCantidad.test(cantidad) && cantidad.length > 0 ? ("error"):("")}>
            <Form.Label>Cantidad</Form.Label>
            <Form.Control
              value={cantidad}
              onChange={e => setCantidad(e.target.value)}
              type="text"
              name="cantidad"
            />
          </Form.Group>
          <Button block bsSize="large" disabled={!validateForm()} type="submit">
            Crear
        </Button>
        </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos insertados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}