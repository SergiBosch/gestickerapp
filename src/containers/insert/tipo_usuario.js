import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Login(props) {
  const [descripcion, setDescripcion] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/tipousuario/get/${params.id}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(login => {
        setDescripcion(login.descripcion)
      }).catch(err => err);
    }

    onLoad();
  }, [props]);

  

  function validateForm() {
    return descripcion.length > 0;
  }

  function handleSubmit(event) {
    const { match: { params } } = props;
    event.preventDefault();
    var data = {
      id: params.id,
      descripcion: descripcion,
    }
    console.log(data)
    fetch(`http://localhost:8082/tipousuario/`, {
      method: 'POST',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div className="Login">
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="descripcion" bsSize="large">
            <Form.Label>Descripcion</Form.Label>
            <Form.Control
              value={descripcion}
              onChange={e => setDescripcion(e.target.value)}
              type="text"
              name="descripcion"
            />
          </Form.Group>
          <Button block bsSize="large" disabled={!validateForm()} type="submit">
            Actualizar
        </Button>
        </Form>

      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos insertados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}

    </div>
  );
}