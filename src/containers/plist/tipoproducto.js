import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";


class servicio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    fetch(`http://localhost:8082/tipoproducto/getpage/${params.page - 1}/${params.rpp}`)
      .then((response) => {
        return response.json()
      })
      .then((servicios) => {
        this.setState({ filas: servicios })
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="tipoproducto" paginaActual={this.state.page} rpp={this.state.rpp}/>
          </div>
          <div className="col-md-2">
            <Rpp tabla="tipoproducto" rpp={this.state.rpp}/>
          </div>
        </div>
        <br />

        <table className="table">
          <thead>
            <tr>
              <th>Descripción</th>
              <th>Productos</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(servicio => {
              return (
                <tr key={servicio.id}>
                  <td>{servicio.descripcion}</td>
                  <td>{servicio.productos}</td>
                  <td>
                    <div className="btn-group">
                      <Link className = "btn btn-default" to={{ pathname: `/delete/tipoproducto/${servicio.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className = "btn btn-default" to={{ pathname: `/update/tipoproducto/${servicio.id}` }}><i className="fas fa-edit"></i></Link>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

}

export default servicio;