import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table'

class tipousuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    fetch('http://localhost:8082/tipousuario/getall')
      .then((response) => {
        return response.json()
      })
      .then((art) => {
        this.setState({ filas: art })
      })    
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="tipousuario" paginaActual={this.state.page} rpp={this.state.rpp}/>
          </div>
          <div className="col-md-2">
            <Rpp tabla="tipousuario" rpp={this.state.rpp}/>
          </div>
        </div>
        <br />
        <Table striped bordered hover>
        <thead>
          <tr>
            <th scope="col">Descripcion</th>
            <th scope="col">Usuarios</th> 
            <th scope="col">Opciones</th>                   
          </tr>
        </thead>
        <tbody>  
          {this.state.filas.map(fila => {
            return (
              <tr key={fila.id}>
                <td>{fila.descripcion}</td>
                <td>{fila.usuarios}</td>
                <td>
                    <div className="btn-group">
                      <Link className = "btn btn-secondary" to={{ pathname: `/delete/tipousuario/${fila.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className = "btn btn-secondary" to={{ pathname: `/update/tipousuario/${fila.id}` }}><i className="fas fa-edit"></i></Link>
                    </div>
                  </td>
              </tr>
            );
          })}
        </tbody>
        </Table>
      </div>
    );
  }
   
}

export default tipousuario;