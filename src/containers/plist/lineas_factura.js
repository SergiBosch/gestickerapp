import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";


class lineasfactura extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    this._isMounted = true;
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this._isMounted = true;
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  componentWillUnmount(){
    this._isMounted = false;
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    fetch(`http://localhost:8082/lineafactura/getpage/${params.page - 1}/${params.rpp}`)
      .then((response) => {
        return response.json()
      })
      .then((lineasfactura) => {
        console.log(lineasfactura)
        this.setState({ filas: lineasfactura })
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="lineafactura" paginaActual={this.state.page} rpp={this.state.rpp}/>
          </div>
          <div className="col-md-2">
            <Rpp tabla="lineafactura" rpp={this.state.rpp}/>
          </div>
        </div>
        <br />

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Factura</th>
              <th scope="col">Producto</th>
              <th scope="col">Tipo Producto</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Precio</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(lineasfactura => {
              return (
                <tr key={lineasfactura.id}>
                  <td scope="row">{lineasfactura.factura.id}</td>
                  <td>{lineasfactura.producto.descripcion}</td>
                  <td>{lineasfactura.producto.tipo_producto.descripcion}</td>
                  <td>{lineasfactura.cantidad}</td>
                  <td>{lineasfactura.precio}</td>
                  <td>
                    <div className="btn-group">
                      <Link className = "btn btn-default" to={{ pathname: `/delete/lineafactura/${lineasfactura.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className = "btn btn-default" to={{ pathname: `/view/lineafactura/${lineasfactura.id}` }}><i className="fas fa-eye"></i></Link>
                      <Link className = "btn btn-default" to={{ pathname: `/update/lineafactura/${lineasfactura.id}` }}><i className="fas fa-edit"></i></Link>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default lineasfactura;