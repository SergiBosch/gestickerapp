import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table'

class usuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    fetch(`http://localhost:8082/usuario/getpage/${params.page - 1}/${params.rpp}`)
      .then((response) => {
        return response.json()
      })
      .then((usuarios) => {
        this.setState({ filas: usuarios })
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="usuario" paginaActual={this.state.page} rpp={this.state.rpp} />
          </div>
          <div className="col-md-2">
            <Rpp tabla="usuario" rpp={this.state.rpp} />
          </div>
        </div>
        <br />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th scope="col">DNI</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Email</th>
              <th scope="col">Tel.</th>
              <th scope="col">Username</th>
              <th scope="col">Tipo usuario</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(usuarios => {
              return (
                <tr key={usuarios.id}>
                  <td scope="row">{usuarios.dni}</td>
                  <td>{usuarios.nombre}</td>
                  <td>{usuarios.apellido1} {usuarios.apellido2}</td>
                  <td>{usuarios.email}</td>
                  <td>{usuarios.telefono}</td>
                  <td>{usuarios.login}</td>
                  <td>{usuarios.tipo_usuario.descripcion}</td>
                  <td>
                    <div className="btn-group">
                      <Link className="btn btn-secondary" to={{ pathname: `/delete/usuario/${usuarios.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className="btn btn-secondary" to={{ pathname: `/view/usuario/${usuarios.id}` }}><i className="fas fa-eye"></i></Link>
                      <Link className="btn btn-secondary" to={{ pathname: `/update/usuario/${usuarios.id}` }}><i className="fas fa-edit"></i></Link>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default usuario;