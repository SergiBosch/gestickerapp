import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import "./ticket.css";
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Pdf from "../PDFCreator";


class ticket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    console.log(this.props.tipo_usuario)
    fetch(`http://localhost:8082/facturaticket/getpage/${params.page - 1}/${params.rpp}`)
      .then((response) => {
        return response.json()
      })
      .then((tickets) => {
        this.setState({ filas: tickets })
      })
  }
  
  render() {
    console.log(this.props.tipo_usuario)
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="facturaticket" paginaActual={this.state.page} rpp={this.state.rpp} />
          </div>
          <div className="col-md-2">
            <Rpp tabla="facturaticket" rpp={this.state.rpp} />
          </div>
        </div>
        <br />

        
        {this.props.tipo_usuario === 3 ? (<>
          {this.state.filas.map(ticket => {
          return (
            <Link style={{ textDecoration: 'none', color: 'black' }}  key={ticket.id} to={{ pathname: `/view/facturaticket/${ticket.id}` }}>
            <div className="panel panel-primary">
              <div className="panel-heading">
          <p className="panel-title">{ticket.titulo}</p>
              </div>
              <div className="panel-body">
                <p>Cliente: {ticket.cliente.nombre} {ticket.cliente.apellido1} {ticket.cliente.apellido2}</p>
                <p>{ticket.descripcion}</p>
              </div>
              <div className="panel-footer">
                Fecha alta: {new Date(ticket.fecha_alta).toLocaleDateString()}
              </div>
            </div>
            </Link>
          );
        })}
        </>) : (<>
          <Table striped bordered hover>
          <thead>
            <tr>
              <th>Titulo</th>
              <th>Fecha Inicio</th>
              <th>Fecha Fin</th>
              <th>Cliente</th>
              <th>Trabajador</th>
              <th>Operaciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(ticket => {
              return (
                <tr key={ticket.id}>
                  <td>{ticket.titulo}</td>
                  <td>{new Date(ticket.fecha_alta).toLocaleDateString()}</td>
                  <td>{new Date(ticket.fecha_fin).toLocaleDateString()}</td>
                  <td>{ticket.cliente.nombre} {ticket.cliente.apellido1} {ticket.cliente.apellido2}</td>
                  <td>{ticket.trabajador.nombre} {ticket.trabajador.apellido1} {ticket.trabajador.apellido2}</td>
                  <td>
                      <Link className="btn btn-secondary" to={{ pathname: `/delete/facturaticket/${ticket.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className="btn btn-secondary" to={{ pathname: `/view/facturaticket/${ticket.id}` }}><i className="fas fa-eye"></i></Link>
                      <Link className="btn btn-secondary" to={{ pathname: `/update/facturaticket/${ticket.id}` }}><i className="fas fa-edit"></i></Link>
                      <Pdf id={ticket.id} ticket={ticket}></Pdf>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        </>)}
        
      </div>
    );
  }

}

export default ticket;