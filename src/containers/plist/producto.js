import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table'

class prodcuto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    fetch(`http://localhost:8082/producto/getpage/${params.page - 1}/${params.rpp}`)
      .then((response) => {
        return response.json()
      })
      .then((materiales) => {
        this.setState({ filas: materiales })
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="producto" paginaActual={this.state.page} rpp={this.state.rpp}/>
          </div>
          <div className="col-md-2">
            <Rpp tabla="producto" rpp={this.state.rpp}/>
          </div>
        </div>
        <br />

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Descripción</th>
              <th>Precio</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(material => {
              return (
                <tr key={material.id}>
                  <td>{material.descripcion}</td>
                  <td>{material.precio}€</td>
                  <td>
                    <div className="btn-group">
                      <Link className = "btn btn-secondary" to={{ pathname: `/delete/producto/${material.id}` }}><i className="fas fa-trash"></i></Link>
                      <Link className = "btn btn-secondary" to={{ pathname: `/view/producto/${material.id}` }}><i className="fas fa-eye"></i></Link>
                      <Link className = "btn btn-secondary" to={{ pathname: `/update/producto/${material.id}` }}><i className="fas fa-edit"></i></Link>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }

}

export default prodcuto;