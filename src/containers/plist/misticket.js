import React, { Component } from 'react';
import { Link } from "react-router-dom";
import "./ticket.css";


class misticket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    if (this.props.isAuthenticated) {
      fetch(`http://localhost:8082/facturaticket/trabajador/${this.props.isAuthenticated.id}`)
        .then((response) => {
          return response.json()
        })
        .then((tickets) => {
          this.setState({ filas: tickets })
        })
    }
  }

  render() {
    return (
      <div>
        {this.state.filas.map(ticket => {
          return (
            <Link style={{ textDecoration: 'none', color: 'black' }} key={ticket.id} to={{ pathname: `/view/facturaticket/${ticket.id}` }}>
              <div className="card">
                <h5 className="card-header">{ticket.titulo}</h5>
                <div className="card-body">
                  <p class="blockquote-footer">Cliente: {ticket.cliente.nombre} {ticket.cliente.apellido1} {ticket.cliente.apellido2}</p>
                  <p>{ticket.descripcion}</p>
                </div>
                <div className="card-footer text-muted">
                  Fecha alta: {new Date(ticket.fecha_alta).toLocaleDateString()}
                </div>
              </div>
            </Link>
          );
        })}
        {this.state.filas.length < 1 ? (<h2>Ahora mismo no tienes ningun ticket asignado</h2>) : ("")}

      </div>
    );
  }

}

export default misticket;