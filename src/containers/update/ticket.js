import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from 'react-select';

import "react-datepicker/dist/react-datepicker.css";
import "./datepicker.css";



export default function Login(props) {
  const [fechainicio, setFechainicio] = useState(new Date());
  const [fechafin, setFechafin] = useState("");
  const [titulo, setTitulo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [cliente, setCliente] = useState("");
  const [nombreCliente, setNombreCliente] = useState("");
  const [trabajador, setTrabajador] = useState("");
  const [nombreTrabajador, setNombreTrabajador] = useState("");
  const [valoresSelect, setValoresselect] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/facturaticket/get/${params.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        setTitulo(login.titulo)
        setDescripcion(login.descripcion)
        setFechainicio(new Date(login.fecha_alta))
        setFechafin(new Date(login.fecha_fin))  
        setCliente(login.cliente.id)   
        setNombreCliente(login.cliente.nombre+" "+login.cliente.apellido1+" "+login.cliente.apellido2)
        setTrabajador(login.trabajador.id)   
        setNombreTrabajador(login.trabajador.nombre+" "+login.trabajador.apellido1+" "+login.trabajador.apellido2)
          
      }).catch(err => err);
    }

    async function valorSelect() {
      fetch(`http://localhost:8082/usuario/getall`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( lista => {
        var itemsSelect = []
        lista.map( elemento =>
          itemsSelect.push({label: elemento.nombre+" "+elemento.apellido1+" "+elemento.apellido2, value: elemento.id})
        )
        setValoresselect(itemsSelect)  
      }).catch(err => err);
    }

    onLoad();
    valorSelect();
  }, [props]);


  /*function validateForm() {
    return cliente.length > 0 && trabajador.length > 0;
  }*/

  function handleSubmit(event) {
    const { match: { params } } = props;
    event.preventDefault();
    var data = {
      id: params.id,
      titulo: titulo,
      descripcion: descripcion,
      fechainicio: fechainicio.toString(),
      fechafin: fechafin.toString(),
      cliente: cliente,
      trabajador: trabajador,
    }
    fetch(`http://localhost:8082/facturaticket/`,{
      method: 'PUT',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
    }).then( response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then( login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      {operacion ? (<>
      <Form onSubmit={handleSubmit}>
        <div className="row">
        <Form.Group controlId="fechainicio" bsSize="large" className="col-md-12">
          <Form.Label>Titulo</Form.Label>
          <Form.Control
          value={titulo}
            onChange={e => setTitulo(e.target.value)}
            type="text"
            name="fechainicio"
          />
        </Form.Group>
        </div>
        <div className="row">
        <Form.Group controlId="cliente" bsSize="large" className="col-md-6">
          <Form.Label>Cliente</Form.Label>
          <Select placeholder={nombreCliente} options={valoresSelect} onChange={e => setCliente(e.value)}/>
        </Form.Group>
        <Form.Group controlId="trabajador" bsSize="large" className="col-md-6">
          <Form.Label>Trabajador Asignado</Form.Label>
          <Select placeholder={nombreTrabajador} options={valoresSelect} onChange={e => setTrabajador(e.value)}/>
        </Form.Group>
        </div>
        <div className="row">
        <div className="row">
        <Form.Group controlId="total" bsSize="large" className="col-md-12">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control componentClass="textarea" value={descripcion} onChange={e => setDescripcion(e.target.value)}/>
        </Form.Group>
        </div>
        </div>
        <Button block bsSize="large" type="submit">
          Actualizar
        </Button>
      </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos actualizados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}