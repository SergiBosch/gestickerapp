import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Select from 'react-select';



export default function Login(props) {
  const [descripcion, setDescripcion] = useState("");
  const [precio, setPrecio] = useState("");
  const [tipoproducto, setTipousuario] = useState("");
  const [tipoproductodescripcion, setTipoproductoDescripcion] = useState("");
  const [valoresSelect, setValoresselect] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const validPrecio = RegExp(/^\d+[.,]?\d{0,2}$/i);
  const validString = RegExp(/^[a-zA-Z0-9 ]{2,50}$/i);

  useEffect(() => {
    async function onLoad() {
      const { match: { params } } = props;
      fetch(`http://localhost:8082/producto/get/${params.id}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(login => {
        setTipousuario(login.tipo_producto.id)
        setTipoproductoDescripcion(login.tipo_producto.descripcion)
        setDescripcion(login.descripcion)
        setPrecio(login.precio)
      }).catch(err => err);

      fetch(`http://localhost:8082/tipoproducto/getall`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(lista => {
        var itemsSelect = []
        lista.map(elemento => 
            itemsSelect.push({ label: elemento.descripcion, value: elemento.id })        
        )
        setValoresselect(itemsSelect) 
      }).catch(err => err);
    
    }


    onLoad();
  }, [props]);

  
  function validateForm() {
    return validString.test(descripcion) && validPrecio.test(precio) && tipoproducto;
  }

  function handleSubmit(event) {
    const { match: { params } } = props;
    event.preventDefault();
    var data = {
      id: params.id,
      tipo_producto: tipoproducto,
      descripcion: descripcion,
      precio: precio
    }
    console.log(data)
    fetch(`http://localhost:8082/producto/`, {
      method: 'PUT',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      setResultado(login)
      setOperacion(false);
    }).catch(err => err);
  }

  return (
    <div className="Login">
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
        <Form.Group controlId="tipousuario" bsSize="large">
            <Form.Label>Tipo Producto</Form.Label>
            <Select placeholder={tipoproductodescripcion} options={valoresSelect} onChange={e => setTipousuario(e.value)} />
          </Form.Group>
          <Form.Group controlId="descripcion" bsSize="large"  validationState={!validString.test(descripcion) && descripcion.length > 0 ? ("error"):("")}>
            <Form.Label>Descripcion</Form.Label>
            <Form.Control
              value={descripcion}
              onChange={e => setDescripcion(e.target.value)}
              type="text"
              name="descripcion"
            />
          </Form.Group>
          <Form.Group controlId="precio" bsSize="large" validationState={!validPrecio.test(precio) && precio.length > 0 ? ("error"):("")}>
            <Form.Label>Precio</Form.Label>
            <Form.Control
              value={precio}
              onChange={e => setPrecio(e.target.value)}
              type="text"
              name="precio"
            />
          </Form.Group>
          <Button block bsSize="large" disabled={!validateForm()} type="submit">
            Actualizar
        </Button>
        </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos actualizados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-default" to={{ pathname: `/` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}