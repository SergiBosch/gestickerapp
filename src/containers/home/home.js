import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import "./home.css";

export default function Home(props) {

  const [numTickets, setNumTickets] = useState("");

  useEffect(() => {
    async function onLoad() {
      if(props.isAuthenticated){
      fetch(`http://localhost:8082/facturaticket/countTickets/${props.isAuthenticated.id}`,{
        method: 'GET',
        credentials: "include",
      }).then( response => {
        return response.json();   
      }).then( login => {
        setNumTickets(login) 
      }).catch(err => err);
    }
  }
    onLoad();
  });

  function menuHome() {
    switch (props.tipo_usuario){
      case 2:
        return <div className="row row-cols-6">
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/usuario/1/10` }}>
            <img src="user2.png" height="170px"/>
            <div className="card-body">
              Lista Usuarios
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/insert/usuario` }}>
            <img src="new-user.png"  height="170px"/>
            <div className="card-body">
              Nuevo Usuario
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/mistickets/` }}>
            <img src="ticket-list.png"  height="170px"/>
            <div className="card-body">
              Mis tickets <span className="badge badge-secondary">{numTickets}</span>
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/insert/facturaticket` }}>
            <img src="new-ticket.png"  height="170px"/>
            <div className="card-body">
              Nuevo ticket
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/producto/1/10` }}>
            <img src="product.png"  height="170px"/>
            <div className="card-body">
              Lista Productos
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/insert/producto` }}>
            <img src="new-product.png"  height="170px"/>
            <div className="card-body">
              Nuevo producto
            </div>
          </Link>
        </div>
        </div>;
      case 1:
        return <>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/usuario/1/10` }}>
            <img src="user2.png" alt=""/>
            <div className="caption">
              Lista Usuarios
            </div>
          </Link>
        </div>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/insert/usuario` }}>
            <img src="new-user.png" alt=""/>
            <div className="caption">
              Nuevo Usuario
            </div>
          </Link>
        </div>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/mistickets/` }}>
            <img src="ticket-list.png" alt=""/>
            <div className="caption">
              Mis tickets <span className="badge">{numTickets}</span>
            </div>
          </Link>
        </div>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/insert/facturaticket` }}>
            <img src="new-ticket.png" alt=""/>
            <div className="caption">
              Nuevo ticket
            </div>
          </Link>
        </div>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/producto/1/10` }}>
            <img src="product.png" alt=""/>
            <div className="caption">
              Lista Productos
            </div>
          </Link>
        </div>
        <div className="col-xs-6 col-md-2">
          <Link className="img-thumbnail" to={{ pathname: `/insert/producto` }}>
            <img src="new-product.png" alt=""/>
            <div className="caption">
              Nuevo producto
            </div>
          </Link>
        </div>
        </>;
      case 3:
        return <div className="row row-cols-6">
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/insert/usuario` }}>
            <img src="new-user.png"  height="170px"/>
            <div className="card-body">
              Nuevo Cliente
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/mistickets/` }}>
            <img src="ticket-list.png"  height="170px"/>
            <div className="card-body">
              Mis tickets <span className="badge badge-secondary">{numTickets}</span>
            </div>
          </Link>
        </div>
        <div className="col">
          <Link className="card" style={{ textDecoration: 'none', color: "black" }} to={{ pathname: `/insert/facturaticket` }}>
            <img src="new-ticket.png"  height="170px"/>
            <div className="card-body">
              Nuevo ticket
            </div>
          </Link>
        </div>
        </div>;
      default:
        return <h2>Por favor, inicie sesion</h2>;
    }
  }


  return (
    <div className="Home">
      {menuHome()}
    </div>
  );
}