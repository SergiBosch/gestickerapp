import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";
import Home from "./containers/home/home";
import NotFound from "./containers/notfound/notfound";
import Login from "./containers/login/login";
import TipoUsuario from "./containers/plist/tipo_usuario";
import Usuario from "./containers/plist/usuario";
import Producto from "./containers/plist/producto";
import Servicio from "./containers/plist/tipoproducto";
import Ticket from "./containers/plist/ticket";
import MisTicket from "./containers/plist/misticket";
import LineasFactura from "./containers/plist/lineas_factura"
import Delete from "./containers/delete/remove";
import UpdateProducto from "./containers/update/producto";
import UpdateServicio from "./containers/update/tipoproducto";
import UpdateTipoUsuario from "./containers/update/tipo_usuario";
import UpdateUsuario from "./containers/update/usuario";
import UpdateTicket from "./containers/update/ticket";
import ViewProducto from "./containers/view/producto";
import ViewServicio from "./containers/view/tipoproducto";
import ViewTipoUsuario from "./containers/view/tipo_usuario";
import ViewUsuario from "./containers/view/usuario";
import ViewTicket from "./containers/view/ticket";
import InsertTicket from "./containers/insert/ticket";
import InsertUsuario from "./containers/insert/usuario";
import InsertProducto from "./containers/insert/producto";
import InsertLineaFactura from "./containers/insert/linea_factura";
import Pdf from "./containers/PDFCreator";

export default function Routes({ appProps }) {
  return (
    <Switch>
      <AppliedRoute path="/" exact component={Home} appProps={appProps}/>
      <AppliedRoute path="/pdf" exact component={Pdf} appProps={appProps}/>
      <AppliedRoute path="/login" exact component={Login} appProps={appProps}/>
      <AppliedRoute path="/tipousuario/:page/:rpp" exact component={TipoUsuario} appProps={appProps}/>
      <AppliedRoute path="/usuario/:page/:rpp" exact component={Usuario} appProps={appProps}/>
      <AppliedRoute path="/producto/:page/:rpp" exact component={Producto} appProps={appProps}/>
      <AppliedRoute path="/tipoproducto/:page/:rpp" exact component={Servicio} appProps={appProps}/>
      <AppliedRoute path="/mistickets/" exact component={MisTicket} appProps={appProps}/>
      <AppliedRoute path="/facturaticket/:page/:rpp" exact component={Ticket} appProps={appProps}/>
      <AppliedRoute path="/lineafactura/:page/:rpp" exact component={LineasFactura} appProps={appProps}/>
      <AppliedRoute path="/delete/:tabla/:id" exact component={Delete} appProps={appProps}/>
      <AppliedRoute path="/update/producto/:id" exact component={UpdateProducto} appProps={appProps}/>
      <AppliedRoute path="/update/tipoproducto/:id" exact component={UpdateServicio} appProps={appProps}/>
      <AppliedRoute path="/update/tipousuario/:id" exact component={UpdateTipoUsuario} appProps={appProps}/>
      <AppliedRoute path="/update/usuario/:id" exact component={UpdateUsuario} appProps={appProps}/>
      <AppliedRoute path="/update/facturaticket/:id" exact component={UpdateTicket} appProps={appProps}/>
      <AppliedRoute path="/view/producto/:id" exact component={ViewProducto} appProps={appProps}/>
      <AppliedRoute path="/view/tipoproducto/:id" exact component={ViewServicio} appProps={appProps}/>
      <AppliedRoute path="/view/tipousuario/:id" exact component={ViewTipoUsuario} appProps={appProps}/>
      <AppliedRoute path="/view/usuario/:id" exact component={ViewUsuario} appProps={appProps}/>
      <AppliedRoute path="/view/facturaticket/:id" exact component={ViewTicket} appProps={appProps}/>
      <AppliedRoute path="/insert/facturaticket" exact component={InsertTicket} appProps={appProps}/>
      <AppliedRoute path="/insert/usuario" exact component={InsertUsuario} appProps={appProps}/>
      <AppliedRoute path="/insert/producto" exact component={InsertProducto} appProps={appProps}/>
      <AppliedRoute path="/insert/lineafactura/:idfactura" exact component={InsertLineaFactura} appProps={appProps}/>
      <Route component={NotFound} />
    </Switch>
  );
}