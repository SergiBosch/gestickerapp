export const signUp = (data) => {
    return fetch({
      method: 'POST',
      url: `http://localhost:8082/session/`,
      headers: {
        'Content-Type': 'application/json',
      },
      body: data,
      credentials: "include",
    });
  };
  